import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Eduardo Alarcon
 * @version  Homework 4 Exercise 1: GUI Calculator
 */

public class CalculatorFrame extends JFrame 
{

	private JPanel contentPane;
	private JTextField lcdjTextField;
	StringBuilder lcdBuffer = new StringBuilder();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() {
			public void run() 
			{
				try 
				{
					CalculatorFrame frame = new CalculatorFrame();
					frame.setVisible(true);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				} //end try-catch block
			} //end run method
		});
	} //end main method

	/**
	 * Create the frame.
	 */
	public CalculatorFrame() 
{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		lcdjTextField = new JTextField();
		contentPane.add(lcdjTextField, BorderLayout.NORTH);
		lcdjTextField.setColumns(10);
		
		JPanel keypadJPanel = new JPanel();
		contentPane.add(keypadJPanel, BorderLayout.CENTER);
		keypadJPanel.setLayout(new GridLayout(4, 4, 5, 5));
		
		JButton btnNewButton = new JButton("7");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("7");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("8");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("8");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("9");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("9");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("/");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("/");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("4");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("4");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("5");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("5");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("6");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("6");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("*");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("*");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("1");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("1");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("2");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("2");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("3");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("3");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("-");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("-");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("0");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("0");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton(".");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append(".");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_13);
		
		JButton btnNewButton_14 = new JButton("=");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("=");
			lcdjTextField.setText( lcdBuffer.toString() );
		});		
		keypadJPanel.add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("+");
		btnNewButton.addActionListener(e -> 
		{
			lcdBuffer.append("+");
			lcdjTextField.setText( lcdBuffer.toString() );
		});
		keypadJPanel.add(btnNewButton_15);
		
	} //end CalculatorFrame constructor 

} //end CalculatorFrame class
